package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.sql.Timestamp;
import java.util.Collections;


class threadControllerTests {
    private Thread stubThread;

    @BeforeEach
    @DisplayName("thread creation test")
    void createThreadTest() {
        stubThread = new Thread(
                "Rish",
                new Timestamp(2121348928345L),
                "forum",
                "message",
                "mlp",
                "title",
                12
        );
    }

    @Test
    @DisplayName("Correct thread creation test")
    void correctlyCreatesThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mlp"))
                    .thenReturn(stubThread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(Collections.emptyList()), controller.createPost("mlp", Collections.emptyList()), "Creating with empty posts");

            assertEquals(stubThread, ThreadDAO.getThreadBySlug("mlp"));
        }
    }

    @Test
    @DisplayName("Getting non-exist slug")
    void notExistSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            assertNull(controller.CheckIdOrSlug("no-exist-slug"), "Non existing slug");
        }
    }

    @Test
    @DisplayName("Get posts")
    void gettingPostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mlp"))
                    .thenReturn(stubThread);

            stubThread.setId(1);

            threadMock.when(() -> ThreadDAO.getPosts(stubThread.getId(), 15, 3, "no", true))
                    .thenReturn(Collections.emptyList());


            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts("mlp", 15, 3, "no", true), "not existing slug");
        }
    }

    @Test
    @DisplayName("Remove thread")
    void removedByChangeTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mlp"))
                    .thenReturn(stubThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("1"))
                    .thenReturn(null);

            threadController controller = new threadController();

            stubThread.setId(1);

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(Collections.emptyList()), controller.createPost("mlp", Collections.emptyList()), "Creating with empty posts");

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(null), controller.change("mlp", null), "delete");
        }
    }

    @Test
    @DisplayName("get Info thread")
    void getInfoTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mlp"))
                    .thenReturn(stubThread);


            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(Collections.emptyList()), controller.createPost("mlp", Collections.emptyList()), "Creating with empty posts");

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(stubThread), controller.info("mlp"), "get info");
        }
    }

    @Test
    @DisplayName("Vote create")
    void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mlp"))
                    .thenReturn(stubThread);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User stubUser = new User("ggboy", "s", "s", "s");

                userMock.when(() -> UserDAO.Info("ggboy"))
                        .thenReturn(stubUser);

                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(stubThread), controller.createVote("mlp", new Vote(stubUser.getNickname(), 1)), "Passing an object of class vote, which is actually null");
            }
        }
    }

}
